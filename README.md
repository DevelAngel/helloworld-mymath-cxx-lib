# HelloWorld MyMath CXX-Lib

Small C++ Library which provides some math functions and/or classes.

## Build

```sh
cmake -S . -B build
cmake --build build
```
