#ifndef MYMATH_H
#define MYMATH_H

namespace MyMath {

int add(int x, int y);
int sub(int x, int y);
int mul(int x, int y);


} // namespace

#endif /* MYMATH_H */
